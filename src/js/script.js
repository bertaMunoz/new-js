let square = new Square()      // on crée une instance de Square

let playfield = document.querySelector('#playfield');


document.body.addEventListener('keydown', function (event) {

  square.moveRight();    //on appelle la méthode moveRight de square
  console.log(square);
  playfield.innerHTML = ''; //remet le contenu de la div playfield à zéro à chaque fois
  playfield.appendChild(square.draw());
});





//  10) En dessous de l'appel de moveRight(), remettre à zéro le contenu de playfield (avec innerHTML = '')(modifié)
// 11) En dessous de la remise à zéro, faire sur le playfield un appendChild de la méthode draw() du carré
