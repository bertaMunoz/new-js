let div = document.querySelector('.red-square');
let body = document.querySelector('body');       //on déclare la variable body 
      //on peut aussi faire document.body.addEventListener (dans ce cas on n'a pas besoin de déclarer la variable body)

body.addEventListener('keydown', function (event) {  /// sur le body, donc ça marchera où que l'on se trouve sur la page. 

  let left = div.offsetLeft;
  left++                           //on incrémente
  div.style.left=`${left}px`;       //on récupère la valeur incrémentée et on l'envoi à la variable left déclarée au dessus.      on peut aussi écrire: div.style.left = left+px
  /////on aurait pu aussi ne pas déclarer left ++ et mettre directement dans div.style.left=`${left++}px`



});


